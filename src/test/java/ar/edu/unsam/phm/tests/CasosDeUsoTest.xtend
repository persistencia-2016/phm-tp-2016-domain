package ar.edu.unsam.phm.tests

import ar.edu.unsam.phm.builders.AsientoBuilder
import ar.edu.unsam.phm.builders.EscalaBuilder
import ar.edu.unsam.phm.builders.UsuarioBuilder
import ar.edu.unsam.phm.builders.VueloBuilder
import ar.edu.unsam.phm.domain.Asiento
import ar.edu.unsam.phm.domain.Escala
import ar.edu.unsam.phm.domain.TarifaBandaNegativa
import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CasosDeUsoTest {
	
	protected Asiento asientoFila1Pasillo
	protected Asiento asientoFila1Centro
	protected Asiento asientoFila1Ventana
	protected Asiento asientoFila2Pasillo
	protected Asiento asientoFila2Centro
	protected Asiento asientoFila2Ventana
	protected Vuelo vueloBsAsRoma
	protected Escala escalaEnBrasil
	protected Usuario usuarioToja
	protected Usuario usuarioJuan

	@Before
	def void init(){

		usuarioToja = new UsuarioBuilder().nick("Tojol").nombre("Toja").password("T").build()
		usuarioJuan = new UsuarioBuilder().nick("Juan").nombre("Juan").password("J").build()

		escalaEnBrasil = new EscalaBuilder().destinoIntermedio("Sao Paulo").horaLlegada("17:00:00").horaSalida("18:30:00").build()
	
		asientoFila1Pasillo = new AsientoBuilder().fila(1).precio(3500.0).tarifa(new TarifaBandaNegativa()).pasillo().build()
		asientoFila1Centro= new AsientoBuilder().fila(1).precio(3500.0).tarifa(new TarifaBandaNegativa()).centro().build()
		asientoFila1Ventana = new AsientoBuilder().fila(1).precio(3500.0).tarifa(new TarifaBandaNegativa()).ventanilla().build()
		asientoFila2Pasillo = new AsientoBuilder().fila(2).precio(3000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build()
		asientoFila2Centro = new AsientoBuilder().fila(2).precio(3000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build()
		asientoFila2Ventana = new AsientoBuilder().fila(2).precio(3000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build()

		vueloBsAsRoma = new VueloBuilder()
							.origen("Buenos Aires")
							.destino("Roma")
							.aerolinea("United Airlines")
							.asiento(asientoFila1Pasillo)
							.asiento(asientoFila1Centro)
							.asiento(asientoFila1Ventana)
							.asiento(asientoFila2Pasillo)
							.asiento(asientoFila2Centro)
							.asiento(asientoFila2Ventana)
							.fechaSalida("28/06/2017 14:15:00")
							.fechaArribo("29/06/2017 10:15:00")
							.escala(escalaEnBrasil)
							.build()
							
		usuarioJuan.reservarAsiento(vueloBsAsRoma, asientoFila1Pasillo)
		
	}
	
	@Test
	def void reservarAsiento() {
		usuarioJuan.reservarAsiento(vueloBsAsRoma, asientoFila2Pasillo)
		Assert.assertTrue(asientoFila1Pasillo.estoyReservado())
	}
	
	@Test
	def void asientoNoSePuedeReservarMas() {
		Assert.assertTrue(asientoFila1Pasillo.estoyReservado())
	}
	
		@Test
	def void asientosExistenEnReserva() {
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoFila2Pasillo)
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoFila2Centro)
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoFila2Ventana)
		Assert.assertEquals(3, usuarioToja.getReservas.size())
	}
	
	@Test
	def void avionDevuelveEnReserva() {
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoFila2Pasillo)
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoFila2Centro)
		Assert.assertEquals(4, vueloBsAsRoma.cantidadDeAsientosLibres)
	}
	
	@Test
	def void asientoNoEstaReservado() {
		Assert.assertFalse(asientoFila1Pasillo.estoyReservado())
	}
	
		
}