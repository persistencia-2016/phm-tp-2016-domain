package ar.edu.unsam.phm.builders

import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.exceptions.BusinessException

class UsuarioBuilder {

	Usuario usuario
		
	new(){
		usuario = new Usuario()
	}		
	
	public def UsuarioBuilder nick(String unNick){
		usuario.setNick(unNick)
		return this
	}

	public def UsuarioBuilder nombre(String unNombre){
		usuario.setNombre(unNombre)
		return this
	}
	
	public def UsuarioBuilder password(String unPass){
		usuario.setPassword(unPass)
		return this
	}
	
	public def Usuario build(){
		return usuario
	}
	
}