package ar.edu.unsam.phm.builders

import ar.edu.unsam.phm.domain.Escala
import ar.edu.unsam.phm.exceptions.BusinessException

class EscalaBuilder {

	Escala escala
	
	new() {
		escala = new Escala()
	}
	
	public def EscalaBuilder destinoIntermedio(String unDestino) {
		escala.setDestinoIntermedio(unDestino)
		return this
	}
	
	public def EscalaBuilder horaLlegada(String hora) {
		escala.setHoraLlegada(hora)
		return this
	}
	
	public def EscalaBuilder horaSalida(String hora) {
		escala.setHoraSalida(hora)
		return this
	}

	private def boolean validarEscala() {
		(this.validarDestinoIntermedio()) && (this.validarHoraLlegada()) && (this.validarHoraSalida())
	}
	
	
	private def boolean validarDestinoIntermedio(){
		escala.destinoIntermedio == null
	}

	private def boolean validarHoraLlegada(){
		escala.horaLlegada == null
	}
	
	private def boolean validarHoraSalida(){
		escala.horaSalida == null
	}
	
	public def Escala build(){
		if (this.validarEscala()){
			throw new BusinessException("Asiento no valido")
		}
		return escala
	}

}