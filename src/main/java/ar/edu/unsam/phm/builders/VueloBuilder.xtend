package ar.edu.unsam.phm.builders

import ar.edu.unsam.phm.domain.Asiento
import ar.edu.unsam.phm.domain.Escala
import ar.edu.unsam.phm.domain.Vuelo
import ar.edu.unsam.phm.exceptions.BusinessException
import java.util.List

class VueloBuilder {

	Vuelo vuelo
		
	new(){
		vuelo = new Vuelo()
	}		
	
	public def VueloBuilder origen(String unOrigen){
		vuelo.setOrigen(unOrigen)
		return this
	}

	public def VueloBuilder destino(String unDestino){
		vuelo.setDestino(unDestino)
		return this
	}
	
	public def VueloBuilder aerolinea(String unaAerolinea){
		vuelo.setAerolinea(unaAerolinea)
		return this
	}

	public def VueloBuilder asiento(Asiento unAsiento){
		vuelo.agregarAsiento(unAsiento)
		return this
	}

	public def VueloBuilder asientos(List<Asiento> listaAsientos){
		vuelo.asientos = listaAsientos
		return this
	}

	public def VueloBuilder fechaSalida(String fecha){
		vuelo.setFechaSalida(fecha)
		return this
	}
	
	public def VueloBuilder fechaArribo(String fecha){
		vuelo.setFechaArribo(fecha)
		return this
	}
	

	public def VueloBuilder escala(Escala unaEscala){
		vuelo.agregarEscala(unaEscala)
		return this
	}
	
	
	protected def validarVuelo(){
		(this.validarOrigen) && (this.validarDestino) && (this.validarAerolinea) && (this.validarAsientos) && (this.validarFechas)
	}
	
	protected def validarOrigen(){
		vuelo.origen.equals(null)
	}
	
	protected def validarDestino(){
		vuelo.destino.equals(null)
	}

	protected def validarAerolinea(){
		vuelo.aerolinea.equals(null)
	}
	
	protected def boolean validarAsientos() {
		vuelo.asientos.isEmpty
	}
	
	protected def boolean validarFechas(){
		vuelo.getFechaSalida.before(vuelo.getFechaArribo)
	}

	public def Vuelo build(){
		if (this.validarVuelo()){
			throw new BusinessException("Vuelo no valido")
		}
		return vuelo
	}
	
}