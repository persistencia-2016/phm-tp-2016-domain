package ar.edu.unsam.phm.builders

import ar.edu.unsam.phm.domain.Asiento
import ar.edu.unsam.phm.domain.LugarAsiento
import ar.edu.unsam.phm.domain.Tarifa
import ar.edu.unsam.phm.exceptions.BusinessException

class AsientoBuilder {
	
	Asiento asiento
	
	new() {
		asiento = new Asiento()
	}
	
	public def AsientoBuilder fila(int unaFila) {
		asiento.setMiFila(unaFila)
		return this
	}
	
	public def AsientoBuilder precio(double unPrecio) {
		asiento.setPrecioAsiento(unPrecio)
		return this
	}
	
	public def AsientoBuilder tarifa(Tarifa unaTarifa) {
		asiento.setUnaTarifa(unaTarifa)
		return this
	}
	
	public def AsientoBuilder pasillo() {
		asiento.setMiLugar(LugarAsiento.P);
		return this;
	}
	
	
	public def AsientoBuilder centro() {
		asiento.setMiLugar(LugarAsiento.C);
		return this;
	}
	
	
	public def AsientoBuilder ventanilla() {
		asiento.setMiLugar(LugarAsiento.V);
		return this;
	}
	
	private def boolean validarAsiento(){
		(this.validarfila()) && (this.validarLugar()) && (this.validarPrecio()) && (this.validarTarifa())
	}
	
	private def boolean validarfila(){
		asiento.miFila.equals(null)
	}

	private def boolean validarLugar(){
		asiento.miLugar.equals(null)
	}
	
	private def boolean validarPrecio() {
		asiento.precioAsiento.equals(null)
	}
	
	private def boolean validarTarifa() {
		asiento.unaTarifa.equals(null)
	}
	
	public def Asiento build(){
		if (this.validarAsiento()){
			throw new BusinessException("Asiento no valido")
		}
		return asiento
	}

}