package ar.edu.unsam.phm.exceptions

import java.lang.RuntimeException

class BusinessException extends RuntimeException {
	
	new(String message) {
		super(message)
	}	
	
}