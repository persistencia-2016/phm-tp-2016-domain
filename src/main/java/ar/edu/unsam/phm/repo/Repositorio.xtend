package ar.edu.unsam.phm.repo

import ar.edu.unsam.phm.domain.Filtro
import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import java.util.ArrayList
import java.util.Collection
import java.util.Iterator
import java.util.List
import java.util.Set
import org.uqbar.commons.model.UserException
import java.util.SortedSet

public class Repositorio {

	static final Repositorio instance = new Repositorio()
	
	private List<Usuario> usuarios = new ArrayList<Usuario>
	private List<Vuelo> vuelos = new ArrayList<Vuelo>

	public static String SELECT = "Elegir Ciudad"

	private new() {
		val CasoDeUso datosDelRepo = new CasoDeUso
		vuelos = datosDelRepo.vuelos
		usuarios = datosDelRepo.usuarios
		}
	

	public def static Repositorio getInstance() {
		return instance
	}
	
		public def List<String> getCiudades() {
			val listaCiudades = new ArrayList<String>
			listaCiudades.add = SELECT
			vuelos.forEach[vuelo | if(!(listaCiudades.contains(vuelo.origen))){ listaCiudades.add(vuelo.origen)}
				if(!(listaCiudades.contains(vuelo.destino))){listaCiudades.add(vuelo.destino)}
			]
			return listaCiudades
		}					
	
	
	public def Usuario getUnUsuario(String nick, String password){

		var Usuario usuarioTemporal = this.buscarUsuario(nick, password)
		if(usuarioTemporal == null){
			throw new UserException("Nick y/o Contrasena incorrectos")
		}
		return usuarioTemporal
	}
	
	private def Usuario buscarUsuario(String nick, String pass){
	 		usuarios.findFirst[usuario | usuario.sosEsteUsuario(nick,pass)]
	}
	

	public def List<Vuelo> BuscadorVuelos(Collection<Filtro> filtros){
		var vuelosBuscados = this.vuelos
		var Iterator<Filtro> iterFiltro = filtros.iterator()
		while (iterFiltro.hasNext){
			vuelosBuscados = iterFiltro.next.aplicarFiltro(vuelosBuscados)
		}
		//Falta hacer log de las consultas
		return vuelosBuscados
	}

}