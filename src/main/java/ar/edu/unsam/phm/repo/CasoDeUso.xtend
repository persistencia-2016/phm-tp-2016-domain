package ar.edu.unsam.phm.repo

import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import java.util.ArrayList
import ar.edu.unsam.phm.builders.UsuarioBuilder
import ar.edu.unsam.phm.builders.AsientoBuilder
import ar.edu.unsam.phm.domain.Asiento
import java.util.List
import ar.edu.unsam.phm.builders.VueloBuilder
import ar.edu.unsam.phm.domain.TarifaComun
import ar.edu.unsam.phm.builders.EscalaBuilder
import ar.edu.unsam.phm.domain.TarifaEspecial
import ar.edu.unsam.phm.domain.TarifaBandaNegativa
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class CasoDeUso {
	List<Usuario> usuarios = new ArrayList<Usuario>
	List<Vuelo> vuelos = new ArrayList<Vuelo>

	new() {
		//Usuarios
		val usuarioMiguel = (new UsuarioBuilder().nick("Miguel").nombre("Migue").password("M").build())
		val usuarioToja = (new UsuarioBuilder().nick("Tojol").nombre("Toja").password("T").build())
		val usuarioJuan = (new UsuarioBuilder().nick("Juan").nombre("Juan").password("J").build())
		
		usuarios.add(usuarioMiguel)
		usuarios.add(usuarioToja)
		usuarios.add(usuarioJuan)
		
		
		
		//Vuelo BsAs->Roma (Con escala en Brasil)
		val escalaEnBrasil = new EscalaBuilder().destinoIntermedio("Sao Paulo").horaLlegada("17:00:00").horaSalida("18:30:00").build()

		val List<Asiento> asientosRoma = new ArrayList<Asiento>
		asientosRoma.add(new AsientoBuilder().fila(1).precio(20000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(1).precio(20000.0).tarifa(new TarifaBandaNegativa()).centro().build())

		val asientoARomaFila1Ventana = new AsientoBuilder().fila(1).precio(20000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build()
		asientosRoma.add(asientoARomaFila1Ventana)
		
		asientosRoma.add(new AsientoBuilder().fila(2).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(2).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(2).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(3).precio(20000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(3).precio(20000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(3).precio(20000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(4).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(4).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(4).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(5).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(5).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(5).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(6).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(6).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(6).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(7).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(7).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(7).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(8).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(8).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(8).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(9).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(9).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(9).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		asientosRoma.add(new AsientoBuilder().fila(10).precio(13000.0).tarifa(new TarifaBandaNegativa()).pasillo().build())
		asientosRoma.add(new AsientoBuilder().fila(10).precio(13000.0).tarifa(new TarifaBandaNegativa()).centro().build())
		asientosRoma.add(new AsientoBuilder().fila(10).precio(13000.0).tarifa(new TarifaBandaNegativa()).ventanilla().build())
		
		val vueloBsAsRoma = new VueloBuilder().origen("Buenos Aires").destino("Roma").aerolinea("AlItalia").asientos(asientosRoma).fechaSalida("28/06/2017 14:15:00").fechaArribo("29/06/2017 10:15:00").escala(escalaEnBrasil).build()
		
		//Vuelo a Santiago de Chile
		val List<Asiento> asientosChile = new ArrayList<Asiento>

		asientosChile.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).centro().build())

		asientosChile.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		val asientoASantiagoFila2Pasillo = new AsientoBuilder().fila(2).precio(1500.0).tarifa(new TarifaComun()).ventanilla().build()

		asientosChile.add(asientoASantiagoFila2Pasillo)
		asientosChile.add(new AsientoBuilder().fila(2).precio(1500.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(2).precio(1500.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(4).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(4).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(4).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(6).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(6).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(6).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(8).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(8).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(8).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosChile.add(new AsientoBuilder().fila(10).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosChile.add(new AsientoBuilder().fila(10).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosChile.add(new AsientoBuilder().fila(10).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())

		val vueloSantiago = new VueloBuilder().origen("Buenos Aires").destino("Santiago de Chile").aerolinea("Aerolineas Argentinas").asientos(asientosChile).fechaSalida("25/10/2016 09:30:00").fechaArribo("25/10/2016 12:15:00").build()
		
		//Vuelo BsAs->Manila (Filipinas) con 2 escalas
		val List<Asiento> asientosFilipinas = new ArrayList<Asiento>

		val escalaBrasil = new EscalaBuilder().destinoIntermedio("Sao Paulo").horaLlegada("14:00:00").horaSalida("15:30:00").build()
		val escalaHolanda= new EscalaBuilder().destinoIntermedio("Amsterdam").horaLlegada("02:00:00").horaSalida("04:30:00").build()

		asientosFilipinas.add(new AsientoBuilder().fila(1).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(1).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(1).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(2).precio(20000.0).tarifa(new TarifaEspecial()).ventanilla().build())
	
		val asientoAFilipinasFila2Centro = new AsientoBuilder().fila(2).precio(20000.0).tarifa(new TarifaEspecial()).ventanilla().build()
		asientosFilipinas.add(asientoAFilipinasFila2Centro)	

		asientosFilipinas.add(new AsientoBuilder().fila(2).precio(20000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(3).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(3).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(3).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(4).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(4).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(4).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(5).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(5).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(5).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(6).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(6).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(6).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(7).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(7).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(7).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(8).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(8).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(8).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(9).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(9).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(9).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())
		asientosFilipinas.add(new AsientoBuilder().fila(10).precio(30000.0).tarifa(new TarifaEspecial()).pasillo().build())
		asientosFilipinas.add(new AsientoBuilder().fila(10).precio(30000.0).tarifa(new TarifaEspecial()).centro().build())
		asientosFilipinas.add(new AsientoBuilder().fila(10).precio(30000.0).tarifa(new TarifaEspecial()).ventanilla().build())

		val vueloBsAsFilipinas = new VueloBuilder().origen("Buenos Aires").destino("Manila").aerolinea("Air France").asientos(asientosFilipinas).fechaSalida("05/01/2017 12:30:00").fechaArribo("07/01/2017 07:00:00").escala(escalaBrasil).escala(escalaHolanda).build()

		//Vuelo BsAs->Sao Paulo sin escalas
		val List<Asiento> asientosSaoPaulo = new ArrayList<Asiento>

		asientosSaoPaulo.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(1).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(2).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(2).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(2).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(3).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(4).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(4).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(4).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(5).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(6).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(6).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(6).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(7).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(8).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(8).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(8).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).centro().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(9).precio(3000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(10).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(10).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosSaoPaulo.add(new AsientoBuilder().fila(10).precio(2000.0).tarifa(new TarifaComun()).ventanilla().build())
	
		val vueloBsAsSaoPaulo= new VueloBuilder().origen("Buenos Aires").destino("Sao Paulo").aerolinea("TAM").asientos(asientosSaoPaulo).fechaSalida("10/06/2016 12:30:00").fechaArribo("10/06/2016 16:00:00").build()

		//Vuelo BsAs a Mexico 1 Escala
		val List<Asiento> asientosMexicoDF = new ArrayList<Asiento>

		val escalaEnLima= new EscalaBuilder().destinoIntermedio("Lima").horaLlegada("10:30:00").horaSalida("11:30:00").build()
		asientosMexicoDF.add(new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		
		val asientoAMexicoDFFila1Ventana = new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build()
		asientosMexicoDF.add(asientoAMexicoDFFila1Ventana)
		
		asientosMexicoDF.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosMexicoDF.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())

		val vueloBsAsAMexicoDF = new VueloBuilder().origen("Buenos Aires").destino("Mexico DF").aerolinea("AeroMexico").asientos(asientosMexicoDF).fechaSalida("10/06/2016 05:30:00").fechaArribo("10/06/2016 22:00:00").escala(escalaEnLima).build()

		//Vuelo BsAs a Barcelona sin escalas

		val List<Asiento> asientosBarcelona = new ArrayList<Asiento>

		asientosBarcelona.add(new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosBarcelona.add(new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		asientosBarcelona.add(new AsientoBuilder().fila(1).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(2).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosBarcelona.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		asientosBarcelona.add(new AsientoBuilder().fila(3).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(4).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosBarcelona.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		asientosBarcelona.add(new AsientoBuilder().fila(5).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(6).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosBarcelona.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		asientosBarcelona.add(new AsientoBuilder().fila(7).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(8).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).pasillo().build())
		asientosBarcelona.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).centro().build())
		asientosBarcelona.add(new AsientoBuilder().fila(9).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())
		asientosBarcelona.add(new AsientoBuilder().fila(10).precio(9000.0).tarifa(new TarifaComun()).ventanilla().build())

		
		val vueloBsAsABarcelona = new VueloBuilder().origen("Buenos Aires").destino("Barcelona").aerolinea("Aerolineas Argentina").asientos(asientosBarcelona).fechaSalida("10/06/2016 05:30:00").fechaArribo("10/06/2016 22:00:00").escala(escalaEnLima).build()

		//Agrego vuelos
		vuelos.add(vueloBsAsRoma)
		vuelos.add(vueloSantiago)
		vuelos.add(vueloBsAsFilipinas)
		vuelos.add(vueloBsAsSaoPaulo)
		vuelos.add(vueloBsAsAMexicoDF)		
		vuelos.add(vueloBsAsABarcelona)
				
		//Usuarios con asientos ya reservados
		usuarioJuan.reservarAsiento(vueloBsAsFilipinas, asientoAFilipinasFila2Centro)
		usuarioJuan.reservarAsiento(vueloBsAsAMexicoDF, asientoAMexicoDFFila1Ventana)
		usuarioToja.reservarAsiento(vueloSantiago, asientoASantiagoFila2Pasillo)
		usuarioToja.reservarAsiento(vueloBsAsRoma, asientoARomaFila1Ventana)
		
		
	}

}