package ar.edu.unsam.phm.repo

import ar.edu.unsam.phm.domain.LogConsulta
import java.util.ArrayList
import java.util.List

class RepositorioLogs {

	static final RepositorioLogs instance = new RepositorioLogs()
	
	private List<LogConsulta> logs = new ArrayList<LogConsulta>
	
	private new(){
		
	}
	
	public def static RepositorioLogs getInstance() {
		return instance
	}
	
	public def void agregarConsulta(LogConsulta log){
		logs.add(log)
	}
	
}