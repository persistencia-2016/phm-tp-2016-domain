package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.List

class FiltroCapacidad implements Filtro {
	
	override aplicarFiltro(List<Vuelo> listaVuelos) {
		Lists.newArrayList(listaVuelos.filter[vuelo | vuelo.tenesLugar()])
	}
}