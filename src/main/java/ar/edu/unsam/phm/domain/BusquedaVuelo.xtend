package ar.edu.unsam.phm.domain

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Collection
import java.util.Date
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class BusquedaVuelo extends ParametrosBusqueda{
	
	Date fechaSalida
	Date fechaArribo
	
	def getFiltros(){
		var Collection<Filtro> filtros = new ArrayList<Filtro>
		filtros.add(new FiltroCapacidad)
		this.agregarFiltroDestino(filtros)
		this.agregarFiltroOrigen(filtros)
		this.agregarFiltroFechas(filtros)
		this.agregarFiltroPrecioMax(filtros)
		return filtros
	}
	
	def agregarFiltroDestino(Collection<Filtro> filtros){
		if (chequearVarNoEsNull(destino) &&	chequearDistintoDeElegir(destino)){
			filtros.add(new FiltroDestino(destino))
		} 
	}
	
	def agregarFiltroOrigen(Collection<Filtro> filtros){
		if ((chequearVarNoEsNull(origen))  && chequearDistintoDeElegir(origen)) {
			filtros.add(new FiltroOrigen(origen))
		} 
	}
	
	def agregarFiltroFechas(Collection<Filtro> filtros){
		this.setFechaSalida(filtros)
		this.setFechaArribo(filtros)
	}
	
	def agregarFiltroPrecioMax(Collection<Filtro> filtros){
		if(chequearVarNoEsNull(precioMaximo)) {
			filtros.add(new FiltroPrecioMaximo(precioMaximo))
		}
	}
	
	public def void setFechaSalida(Collection<Filtro> unosFiltros){
		if (chequearVarNoEsNull(fechaS)) {
			fechaSalida = new SimpleDateFormat("dd/MM/yyyy").parse(fechaS)
		}
		else {
			fechaSalida = new Date()
		}
		unosFiltros.add(new FiltroFechaSalida(fechaSalida))
		
	}
	
	public def void setFechaArribo(Collection<Filtro> unosFiltros){
		if (chequearVarNoEsNull(fechaA)) {
			fechaArribo = new SimpleDateFormat("dd/MM/yyyy").parse(fechaA)
			if(fechaSalida.compareTo(fechaArribo)<=0){
				unosFiltros.add(new FiltroFechaArribo(fechaArribo))
			}
			else {
				throw new UserException("Fecha de arribo posterior a la fecha de salida.")
			}
		}
	}
	
	private def chequearVarNoEsNull(Object variable) {
		variable != null
	}
	
	private def chequearDistintoDeElegir(String variable) {
		!(variable.equals("Elegir Ciudad"))
	}
	
	def ParametrosBusqueda devolverParametros(){
		val parametros = new ParametrosBusqueda()
		parametros.setearBusqueda(origen,destino,precioMaximo,fechaS,fechaA)
		return parametros
	}
	
}