package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.Date
import java.util.List

class FiltroFechaArribo implements Filtro{
	
	Date fechaHasta
	
	new(Date fechaHastaBuscada){
		fechaHasta = fechaHastaBuscada
	}
	
	override aplicarFiltro(List<Vuelo> listaVuelos) {
		Lists.newArrayList(listaVuelos.filter[vuelo | (vuelo.fechaArribo.compareTo(fechaHasta) <= 0) ])

	}
	
}