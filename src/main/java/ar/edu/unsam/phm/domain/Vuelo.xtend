package ar.edu.unsam.phm.domain

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Vuelo {
	
	String origen
	String destino	
	String aerolinea
	List<Escala> escalas = new ArrayList<Escala>
	List<Asiento> asientos = new ArrayList<Asiento>
	Date fechaSalida 
	Date fechaArribo
	SimpleDateFormat dateToString = new SimpleDateFormat("dd/MM/yyyy - HH:mm 'hs'")

	public def void setFechaSalida(String fecha){
		fechaSalida = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(fecha)
	}

	public def void setFechaArribo(String fecha){
		fechaArribo = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(fecha)
	}	
	
	public def String getFechaSalidaToStr() { 
		dateToString.format(fechaSalida)
	}
	
	public def String getFechaArriboToStr() {
		dateToString.format(fechaArribo)
	}
	
	
	public def void reservameEsteAsiento(Usuario usuario, Asiento asientoAReservar) {
		asientoAReservar.fuisteReservado(usuario)		
	}
	
	public def void cancelameEsteAsiento(Usuario usuario, Asiento asientoACancelar){
		asientoACancelar.quitarReserva()
	}
	
	public def agregarAsiento(Asiento unAsiento) {
		asientos.add(unAsiento)
	}
	
	def agregarEscala(Escala unaEscala) {
		escalas.add(unaEscala)
	}
	
	public def getCapacidad(){
		return asientos.size()
	}
	
	public def tenesLugar(){
		asientos.exists[asiento | !asiento.estoyReservado]
	}
	
	public def Boolean hayAsientosLibresDeCiertoPrecio(Double precioMaximo){
		this.asientosLibresDeCiertoPrecio(precioMaximo).isEmpty()
	}
	
	public def asientosLibresDeCiertoPrecio(Double precioMaximo) {
		asientos.filter[ asiento | this.precioAsientoExcede(asiento, precioMaximo) ]
	}

	private def Boolean precioAsientoExcede(Asiento unAsiento, Double precioMaximo){
		unAsiento.dameTuPrecio(fechaSalida) >= precioMaximo
	}
		
	public def int cantidadEscalas(){
		escalas.size()
	}
	
	public def int getCantidadDeAsientosLibres() {
		this.getAsientosLibres().size()
	}
	
	public def getAsientosLibres() {
		asientos.filter[asiento | !(asiento.estoyReservado()) ]
	}
	
	public def Boolean esTudestino(String esTuDestino) {
		destino == esTuDestino 
	}
	
}