package ar.edu.unsam.phm.domain

import java.util.List

interface Filtro {
	
	def List<Vuelo> aplicarFiltro(List<Vuelo> listaVuelos)	

}