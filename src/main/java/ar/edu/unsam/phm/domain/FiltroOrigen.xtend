package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.List

class FiltroOrigen implements Filtro{
	
	String origen
	
	new(String origenBuscado){
		origen = origenBuscado
	}
	
	override aplicarFiltro(List<Vuelo> listaVuelos) {
		Lists.newArrayList(listaVuelos.filter[vuelo | vuelo.origen.equals(origen)])
 	}
		
}