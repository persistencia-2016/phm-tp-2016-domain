package ar.edu.unsam.phm.domain

import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors

@Observable
@Accessors
class Reserva {

	public Vuelo vuelo
	Asiento asiento

	new(Vuelo vueloAsignado,Asiento asientoAsignado){
		vuelo = vueloAsignado
		asiento = asientoAsignado
	}
	
	protected def void establecerAsiento(Asiento asientoAReservar){
		asiento = asientoAReservar
	}	
	
	protected def void establecerVuelo(Vuelo vueloAsignado){
		vuelo = vueloAsignado
	}
	
	public def boolean sosEstaReserva(Vuelo vueloAsignado, Asiento asientoAReservar){
		this.sosEsteVuelo(vueloAsignado) && this.sosEsteAsiento(asientoAReservar) 

	}

	private def boolean sosEsteVuelo(Vuelo vueloAsignado) {
		vuelo.equals(vueloAsignado)
	}

	
	private def boolean sosEsteAsiento(Asiento asientoAsignado){
		asiento.equals(asientoAsignado)
	}
	
	public def String getAsientoReservado(){
		return asiento.devolvermeComoString()
		
	}

}