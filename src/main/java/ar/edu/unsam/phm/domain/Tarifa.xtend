package ar.edu.unsam.phm.domain

import java.util.Date

public interface Tarifa {
	
	public def Double getTarifa(Date fechaVuelo, Date fechaReservaAsiento,double precioAsiento)
	
}