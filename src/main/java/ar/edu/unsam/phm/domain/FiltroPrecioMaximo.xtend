package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.List

class FiltroPrecioMaximo implements Filtro{
	
	Double precioMaximo
	
	new(Double precioMaximoBuscado) {
			precioMaximo = precioMaximoBuscado
	}
	
	override aplicarFiltro(List<Vuelo> listaVuelos) {
		Lists.newArrayList(listaVuelos.filter[vuelo|vuelo.hayAsientosLibresDeCiertoPrecio(precioMaximo)])
 	}

}