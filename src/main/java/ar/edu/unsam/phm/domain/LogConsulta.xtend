package ar.edu.unsam.phm.domain

import java.util.ArrayList
import java.util.Date
import java.util.List

class LogConsulta {
	
	ParametrosBusqueda parametros	
	Usuario usuario
	Date fechaConsulta
	List<Vuelo> vuelosObtenidos = new ArrayList<Vuelo>
	
	new(Usuario usuarioTemporal,ParametrosBusqueda parametrosUsados,List<Vuelo> vuelos){
		usuario = usuarioTemporal
		parametros = parametrosUsados
		fechaConsulta = new Date()
		vuelosObtenidos = vuelos
	}
	
	
	
}