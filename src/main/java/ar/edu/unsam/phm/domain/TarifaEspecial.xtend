package ar.edu.unsam.phm.domain

import ar.edu.unsam.phm.domain.Tarifa
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.Date

@Accessors
class TarifaEspecial implements Tarifa {

	double descuento
	
	override getTarifa(Date fechaVuelo, Date fechaReservaAsiento, double precioAsiento) {
		return precioAsiento - this.descuento
	}
	
}