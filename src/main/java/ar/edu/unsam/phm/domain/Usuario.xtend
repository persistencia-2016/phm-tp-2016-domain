package ar.edu.unsam.phm.domain

import ar.edu.unsam.phm.exceptions.BusinessException
import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Usuario {
	
	String nick
	String nombre
	String password
	List<Reserva> reservas = new ArrayList<Reserva>
	
	def reservarAsiento(Vuelo vuelo, Asiento asientoAReservar){
		if(asientoAReservar.noEstoyReservado){
			vuelo.reservameEsteAsiento(this, asientoAReservar)
			reservas.add(new Reserva(vuelo,asientoAReservar))
		}else{
			throw new UserException("Asiento ya reservado")			
		}
	}
	
	def cancelarReserva(Vuelo vuelo, Asiento asientoACancelar){
		vuelo.cancelameEsteAsiento(this, asientoACancelar)
		reservas.remove(this.encontrarReserva(vuelo, asientoACancelar))
	}

	private def Reserva encontrarReserva(Vuelo unVuelo, Asiento asientoACancelar){
		reservas.findFirst[ reserva | reserva.sosEstaReserva(unVuelo, asientoACancelar) ]
	}
	
	def sosEsteUsuario(String nickBuscado,String passwordBuscado){
		this.nickCorrecto(nickBuscado)&& this.passwordCorrecto(passwordBuscado)
	}
	
	def nickCorrecto(String nickBuscado){
		this.nick.equals(nickBuscado)
	}
	
	def passwordCorrecto(String passwordBuscado){
		this.password.equals(passwordBuscado)
	}
	
	protected def soyValido(){
		
		if (!((this.validarNombre) && (this.validarNick) && (this.validarPassword))){
			throw new BusinessException("Usuario no valido")
		}
		
	}
	
	private def validarNick(){
		nick == null
	}
	
	private def validarNombre(){
		nombre == null
	}

	private def validarPassword(){
		password == null
	}
}