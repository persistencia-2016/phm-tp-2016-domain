package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.Date
import java.util.List

class FiltroFechaSalida implements Filtro{
	
	Date fechaSalida
	
	new (Date fechaSalidaBuscada){
		fechaSalida = fechaSalidaBuscada
	}
	
	override aplicarFiltro(List<Vuelo> listaVuelos) {
		Lists.newArrayList(listaVuelos.filter[vuelo | (vuelo.fechaSalida.compareTo(fechaSalida) >= 0) ])
 	}
	
}