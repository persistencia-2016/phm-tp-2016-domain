package ar.edu.unsam.phm.domain

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class ParametrosBusqueda {

	String origen
	String destino
	Double precioMaximo
	String fechaS
	String fechaA
	
	new(){}
	
	def void setearBusqueda(String origenBuscado,String destinoBuscado, Double precioMaximoBuscado,String fechaSBuscada,String fechaABuscada){
		origen = origenBuscado
		destino = destinoBuscado
		precioMaximo = precioMaximoBuscado
		fechaS = fechaSBuscada
		fechaA = fechaABuscada
	}
	
	
}