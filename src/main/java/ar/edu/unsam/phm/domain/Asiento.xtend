package ar.edu.unsam.phm.domain

import java.text.SimpleDateFormat
import java.util.Date
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Asiento {

	int miFila
	LugarAsiento miLugar
	SimpleDateFormat fechaDeReserva = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
	Usuario usuarioQueReservo = null
	double precioAsiento
	Tarifa unaTarifa

	private def void setFechaDeReserva(){
		fechaDeReserva.format(new Date())						
	}	

	protected def void fuisteReservado(Usuario unUsuario){
		this.usuarioQueReservo = unUsuario
		this.setFechaDeReserva
	}
	
	public def boolean estoyReservado(){
		this.usuarioQueReservo != null
	}
	
	public def void quitarReserva(){
		usuarioQueReservo = null
	}
	
	public def double dameTuPrecio(Date fechaSalida){
		unaTarifa.getTarifa(fechaSalida, new Date(), this.precioAsiento)
	}
	
	public def String devolvermeComoString() {
		return miFila.toString() + miLugar.toString()
	}
	
	public def boolean noEstoyReservado(){
		!(this.estoyReservado)
	}
	
}