package ar.edu.unsam.phm.domain

import java.util.Date

class TarifaBandaNegativa implements Tarifa {

	override getTarifa(Date fechaVuelo, Date fechaReservaAsiento, double precioAsiento) {
		if(this.restarDias(fechaVuelo, fechaReservaAsiento)>=3){
			return precioAsiento*0.8
		}
		else{
			return precioAsiento*0.9
		}
	}
	
	private def int restarDias(Date fechaVuelo, Date fechaReservaAsiento) {
		val diferenciaEn_ms = fechaVuelo.getTime() - fechaReservaAsiento.getTime();
		return (diferenciaEn_ms / (1000 * 60 * 60 * 24)) as int
	}

}