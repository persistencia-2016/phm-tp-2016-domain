package ar.edu.unsam.phm.domain

import java.text.SimpleDateFormat
import java.util.Date
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Escala {
	
	String destinoIntermedio
	Date horaLlegada
	Date horaSalida
	SimpleDateFormat dateToString = new SimpleDateFormat("HH:mm 'hs'")

	public def void setHoraLlegada(String hora){
		val horaFormat = new SimpleDateFormat("HH:mm:ss")
		horaLlegada = horaFormat.parse(hora)
	}

	public def void setHoraSalida(String hora){
		val horaFormat = new SimpleDateFormat("HH:mm:ss")
		horaSalida = horaFormat.parse(hora)
	}
	
	public def String getHoraSalidaToStr() { 
		dateToString.format(horaSalida)
	}
	
	public def String getHoraLlegadaToStr() {
		dateToString.format(horaLlegada)
	}

}