package ar.edu.unsam.phm.domain

import com.google.common.collect.Lists
import java.util.List

class FiltroDestino implements Filtro{
	
	String destino
	
	new(String destinoBuscado){
		destino = destinoBuscado
	}

	override aplicarFiltro(List<Vuelo> listaVuelos) {
			Lists.newArrayList (listaVuelos.filter[vuelo | vuelo.esTudestino(destino) ])
 	}
	
}